﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game();
            game.ScoreHandler += ShowScoreInConsole;
            game.FinishHandler += FinishInConsole;

            game.ScoreFirst();
            game.ScoreFirst();
            game.ScoreFirst();
            game.ScoreFirst();
            game.ScoreSecond();
            game.FinishMatch();
        }

        private static void ShowScoreInConsole(object sender, ScoreEventArgs args)
        {
            Console.WriteLine(args.Message);
            Console.WriteLine($"Current score { args.ScoreBoard.First }:{args.ScoreBoard.Second }.\n");
        }

        private static void FinishInConsole(ScoreBoard scoreboard)
        {
            Console.WriteLine("Final score " + scoreboard.First + ":" + scoreboard.Second);
        }
    }
}