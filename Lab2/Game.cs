﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    public class Game
    {
        public delegate void FinishGame(ScoreBoard score);
        public event FinishGame FinishHandler;

        public delegate void ScoreEventHandler(object sender, ScoreEventArgs args);
        public event ScoreEventHandler ScoreHandler;
        public ScoreBoard Scoreboard { get; }
        public Game()
        {
            Scoreboard = new ScoreBoard();
        }

        public void ScoreFirst()
        {
            Scoreboard.First++;
            ScoreHandler.Invoke(this, new ScoreEventArgs("First team scored", Scoreboard));
        }
        public void ScoreSecond()
        {
            Scoreboard.Second++;
            ScoreHandler.Invoke(this, new ScoreEventArgs("Second team scored", Scoreboard));
        }
        public void FinishMatch()
        {
            foreach (Delegate d in ScoreHandler.GetInvocationList())
            {
                ScoreHandler -= (ScoreEventHandler)d;
            }

            FinishHandler.Invoke(Scoreboard);
        }
    }
}
