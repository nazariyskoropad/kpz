﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    public class ScoreBoard
    {
        public int First { get; set; }
        public int Second { get; set; }
        public ScoreBoard()
        {
            First = 0;
            Second = 0;
        }
    }

    public class ScoreEventArgs
    {
        public string Message { get; }
        public ScoreBoard ScoreBoard { get; }
        public ScoreEventArgs(string msg, ScoreBoard score)
        {
            Message = msg;
            ScoreBoard = score;
        }
    }
}
