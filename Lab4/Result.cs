﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4
{
    [Flags]
    public enum Result
    {
        Win = 1, //001
        Lose = 2, //010
        Draw = 4//100
    }
}
