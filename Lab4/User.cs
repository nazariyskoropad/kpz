﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4
{
    public interface IUser
    {
        public void StartGame();
    }

    public interface IVipUser
    {
        public void UsePower();
    }

    public abstract class User : IUser
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        protected Statistics UserStatistics { get; set; }

        public static int UsersCount {get; set; }

        static User()
        {
            UsersCount = 0;
            Console.WriteLine("Static user constructor");
        }

        public User(int Id, string NickName)
        {
            this.Id = Id;
            this.NickName = NickName;
            this.UserStatistics = new User.Statistics();
            Console.WriteLine("Public user constructor");
        }

        protected class Statistics
        {
            public int WinsCount { get; set; }
            public int LosesCount { get; set; }
            public int DrawsCount { get; set; }

            public Statistics()
            {
                WinsCount = 0;
                LosesCount = 0;
                DrawsCount = 0;
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            Random rnd = new Random();
            return rnd.Next(int.MinValue, int.MaxValue);
        }
        public abstract void StartGame();
    }

    public class SimpleUser : User
    {
        public SimpleUser(int Id, string NickName) : base(Id, NickName) 
        {
            Console.WriteLine("Public simple user constructor");
        }
        public override void StartGame()
        {
            Console.WriteLine("Simple user starts game");
        }

        public override string ToString()
        {
            return "Simple user class";
        }
    }

    public class VipUser : User, IVipUser
    {
        public DateTime Birthday { get; set; }

        public VipUser(int Id, string NickName, DateTime birthday) : base(Id, NickName) 
        {
            this.Birthday = birthday;
            Console.WriteLine("Public vip user constructor");
        }

        public override void StartGame()
        {
            Console.WriteLine("Vip user starts game");
        }

        public void UsePower()
        {
            Console.WriteLine("Vip user uses power");
        }

        public override string ToString()
        {
            return "Vip user class";
        }
    }
}
