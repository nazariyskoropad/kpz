﻿using System;
using System.Collections;
using System.Diagnostics;

namespace Lab4
{
    class Program
    {
        public static Result GameResult { get; set; }
        static void Main(string[] args)
        {
            UserTask();
            EnumsTask();
            BoxingTask();
            WithoutBoxingTask();
        }

        private static void UserTask()
        {
            var vipUser = new VipUser(1, "Nazar", DateTime.Now);
            Console.WriteLine("Overriden GetHashCode " + vipUser.GetHashCode());
            Console.WriteLine("Overriden ToString" + vipUser.ToString());
        }

        private static void EnumsTask()
        {
            Console.WriteLine("\nEnum task");

            Console.WriteLine("Results enum:");
            Console.WriteLine(Result.Win + " - " + (int)Result.Win);
            Console.WriteLine(Result.Lose + " - " + (int)Result.Win);
            Console.WriteLine(Result.Draw + " - " + (int)Result.Draw);

            GameResult = Result.Draw | Result.Win | Result.Lose;

            Console.WriteLine("All possible GameResults: " + GameResult);

            GameResult = GameResult ^ Result.Lose;
            Console.WriteLine("GameResults without lose: " + GameResult);

            if ((GameResult & Result.Lose) != Result.Lose)
            {
                Console.WriteLine("Game result is not lose");
            }
            if ((GameResult & Result.Win) != Result.Win)
            {
                Console.WriteLine("Game result is not win");
            }

            Console.WriteLine();

            Result newGameResult;

            GetGameResult(out newGameResult);
            Console.WriteLine("Current game result = " + newGameResult);

            ChangeGameResultWithRef(ref newGameResult);
            Console.WriteLine("Game result after changed using ref = " + newGameResult);

            ChangeGameResultWithoutRef(newGameResult);
            Console.WriteLine("Game result after changed without ref = " + newGameResult);
        }

        private static void GetGameResult(out Result newGameResult)
        {
            newGameResult = Result.Win;
        }

        private static void ChangeGameResultWithRef(ref Result newGameResult)
        {
            newGameResult = Result.Lose;
        }

        private static void ChangeGameResultWithoutRef(Result newGameResult)
        {
            newGameResult = Result.Win;
        }

        private static void BoxingTask()
        {
            object obj;

            System.GC.Collect();
            Console.WriteLine("Boxing 1000000000 floats time");
            var watch3 = new Stopwatch();
            watch3.Start();
            float c = 0;
            for (long i = 0; i < 1000000000; i++)
            {
                obj = c;
                c = (float)obj;
            }
            watch3.Stop();
            Console.WriteLine("Time = " + watch3.ElapsedMilliseconds);

            System.GC.Collect();

            int b = 0;
            Console.WriteLine("Boxing 1000000000 ints time");
            var watch2 = new Stopwatch();
            watch2.Start();
            for (long i = 0; i < 1000000000; i++)
            {
                obj = b;
                b = (int)obj;
            }
            watch2.Stop();
            Console.WriteLine("Time = " + watch2.ElapsedMilliseconds);

            System.GC.Collect();

            Console.WriteLine("Boxing 1000000000 doubles time");
            var watch1 = new Stopwatch();
            watch1.Start();
            double a = 0;
            for (long i = 0; i < 1000000000; i++)
            {
                obj = a;
                a = (double)obj;
            }
            watch1.Stop();
            Console.WriteLine("Time = " + watch1.ElapsedMilliseconds);
        }


        private static void WithoutBoxingTask()
        {
            Console.WriteLine("Without boxing 1000000000 floats time");
            var watch3 = new Stopwatch();
            watch3.Start();
            float c1 = 0;
            float c2 = 0;
            for (long i = 0; i < 1000000000; i++)
            {
                c1 = c2;
            }
            watch3.Stop();
            Console.WriteLine("Time = " + watch3.ElapsedMilliseconds);

            int b1 = 0;
            int b2 = 0;
            Console.WriteLine("Boxing 1000000000 ints time");
            var watch2 = new Stopwatch();
            watch2.Start();
            for (long i = 0; i < 1000000000; i++)
            {
                b1 = b2;
            }
            watch2.Stop();
            Console.WriteLine("Time = " + watch2.ElapsedMilliseconds);

            System.GC.Collect();

            Console.WriteLine("Boxing 1000000000 doubles time");
            var watch1 = new Stopwatch();
            watch1.Start();
            double a1 = 0;
            double a2 = 0;
            for (long i = 0; i < 1000000000; i++)
            {
                a1 = a2;
            }
            watch1.Stop();
            Console.WriteLine("Time = " + watch1.ElapsedMilliseconds);
        }
    }
}
