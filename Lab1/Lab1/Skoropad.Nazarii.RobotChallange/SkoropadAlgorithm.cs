﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Skoropad.Nazarii.RobotChallange
{
    public class SkoropadAlgorithm : IRobotAlgorithm
    {
        public string Author => "Skoropad Nazarii";
        public string trueAuthor => "Skoropad Nazarii";

        public int RoundNumber { get; set; } = 0;
        public int RobotsNumber { get; set; } = 10;
        public bool isRobotNumberMax { get; set; } = false;
        public int PlayersCount { get; set; }
        public bool Initialized { get; set; } = false;
        public int MapPart { get; set; } = 0;
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];

            if (!Initialized)
            {
                PlayersCount = robots.Count / 10;
                Initialized = true;
                MapPart = FindMapPart(movingRobot.Position);
            }

            if(!isRobotNumberMax )
            {
                RobotsNumber = robots.Where(r => r.OwnerName == trueAuthor).Count();
                if (RobotsNumber == 100)
                    isRobotNumberMax = true;
            }
       

            if (robotToMoveIndex < PlayersCount)
                RoundNumber++;


            if (RoundNumber == 1)
            {
                Position stationPos = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
                var distance = DistanceHelper.FindDistance(stationPos, movingRobot.Position);
                if(distance < 25 && isStationPotentiallyFree(stationPos, movingRobot, robots))
                {
                    return new CreateNewRobotCommand() { NewRobotEnergy = 25 };
                }
                else
                {
                    return new MoveCommand() { NewPosition = stationPos };
                }
            }

            if (RoundNumber == 2)
            {
                Position stationPos = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
                var distance = DistanceHelper.FindDistance(stationPos, movingRobot.Position);

                var enemyPos = GetNearestEnemy(stationPos, map, robots);
                var distanceToEnemy = DistanceHelper.FindDistance(enemyPos, stationPos);

                if (CanCollect(stationPos, movingRobot.Position))
                {
                    if (map.Stations.FirstOrDefault(s => s.Position == stationPos).Energy > 20)
                        return new CollectEnergyCommand();
                    else if (distanceToEnemy < 2)
                        return new MoveCommand() { NewPosition = enemyPos };
                }
                else
                {
                    if (distanceToEnemy < 2 && movingRobot.Energy > distance)
                        return new MoveCommand() { NewPosition = enemyPos };
                    else if(movingRobot.Energy > distance)
                        return new MoveCommand() { NewPosition = stationPos };
                    else
                        return new MoveCommand() { NewPosition = GetNewDefaultPosition(movingRobot.Position)
                        };
                }
            }

            if (!isRobotNumberMax)
            {
                Position stationPosition = FindNearestFreeStation(movingRobot, map, robots);
                var distance = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);

                var enemyPos = GetNearestEnemy(stationPosition, map, robots);
                var distanceToEnemy = DistanceHelper.FindDistance(enemyPos, stationPosition);

                if (CanCreate(movingRobot) && distance < 25 && isStationPotentiallyFree(stationPosition, movingRobot, robots))
                {
                    if (distance <= 2)
                        return new CreateNewRobotCommand() { NewRobotEnergy = (movingRobot.Energy - 50) / 2 };
                    else
                        return new CreateNewRobotCommand() { NewRobotEnergy = 60 };
                }
                else if (CanCollect(stationPosition, movingRobot.Position)
                    && map.Stations.FirstOrDefault(s => s.Position == stationPosition).Energy > 20)
                {
                    return new CollectEnergyCommand();
                }
                else
                {
                    if (movingRobot.Energy > distanceToEnemy)
                        return new MoveCommand() { NewPosition = enemyPos };
                    else if (movingRobot.Energy > distance)
                        return new MoveCommand() { NewPosition = stationPosition };
                    else
                        return new MoveCommand() { NewPosition = GetNewDefaultPosition(movingRobot.Position) };
                }
            }
            else
            {
                var myRobots = robots.Where(r => r.OwnerName == trueAuthor).ToList();
                var energies = myRobots.Select(r => r.Energy).ToList();

                Position stationPosition = FindNearestFreeStation(movingRobot, map, robots);
                var distance = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);

                var enemyPos = GetNearestEnemy(stationPosition, map, robots);
                var distanceToEnemy = DistanceHelper.FindDistance(enemyPos, stationPosition);

                if (CanCollect(stationPosition, movingRobot.Position)
                     && map.Stations.FirstOrDefault(s => s.Position == stationPosition).Energy > 20)
                {
                    return new CollectEnergyCommand();
                }
                else if (RoundNumber > 40)
                {
                    var pos = FindVictim(movingRobot, robots);
                    if (pos != null)
                        return new MoveCommand() { NewPosition = pos };
                    else
                    {
                        if (movingRobot.Energy > distanceToEnemy)
                            return new MoveCommand() { NewPosition = enemyPos };
                        else if (movingRobot.Energy > distance)
                            return new MoveCommand() { NewPosition = stationPosition };
                        else
                            return new MoveCommand() { NewPosition = GetNewDefaultPosition(movingRobot.Position) };
                    }
                }
                else
                {
                    if (movingRobot.Energy > distanceToEnemy)
                        return new MoveCommand() { NewPosition = enemyPos };
                    else if (movingRobot.Energy > distance)
                        return new MoveCommand() { NewPosition = stationPosition };
                    else
                        return new MoveCommand() { NewPosition = GetNewDefaultPosition(movingRobot.Position) };
                }
                
            }

        }

        private Position FindVictim(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            var winning = Int32.MinValue;
            Position result = null;
            foreach (var robot in robots)
            {
                if(robot.OwnerName != trueAuthor)
                {
                    var distanceToNearEnemy = DistanceHelper.FindDistance(movingRobot.Position, robot.Position);
                    var spentEnergy = distanceToNearEnemy + 20;
                    var diff = robot.Energy / 10 - spentEnergy;
                    if(diff > winning)
                    {
                        winning = diff;
                        result = robot.Position;
                    }
                }
            }
            return result;
        }

        private Position GetNewDefaultPosition(Position position)
        {
            switch (MapPart)
            {
                case 0: return new Position(position.X + 1, position.Y + 1);
                case 1: return new Position(position.X + 1, position.Y - 1);
                case 2: return new Position(position.X - 1, position.Y + 1);
                default:
                    return new Position(position.X - 1, position.Y - 1); ;
            }
    }

        private int FindMapPart(Position position)
        {
            if (position.X > 50 && position.Y > 50)
                return 3;
            if (position.X > 50 && position.Y < 50)
                return 2;
            if (position.X < 50 && position.Y > 50)
                return 1;
            else
                return 0;
        }

        private bool CanCreate(Robot.Common.Robot movingRobot)
        {
            return !isRobotNumberMax && movingRobot.Energy >= 120;
        }

        private Position GetNearestEnemy(Position startPosition, Map map, IList<Robot.Common.Robot> robots)
        {
            int distance = Int32.MaxValue;
            Position position = new Position();
            foreach (var robot in robots)
            {
                if (robot.OwnerName != trueAuthor)
                {
                    var newDist = DistanceHelper.FindDistance(startPosition, robot.Position);
                    if(newDist < distance )
                    {
                        distance = newDist;
                        position = robot.Position;
                    }
                }
                if (distance <= 2)
                    break;
            }
            return position;
        }

        private bool CanCollect(Position stationPosition, Position robotPosition)
        {
            return DistanceHelper.FindDistance(stationPosition, robotPosition) <= 2;
        }

        //public class Employee
        //{
        //    private string _name;

        //    public string Name
        //    {
        //        get { return Name; }
        //    }

        //var emp = new Employee();
        //var name = emp.Name;

        //return new CollectEnergyCommand();
        //}

        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
                   IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
                    IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            var robotsNumberNear = 0;
            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.OwnerName == trueAuthor)
                {
                    var distance = DistanceHelper.FindDistance(robot.Position, cell);
                    if (distance <= 2)
                        robotsNumberNear++;
                }
            }
            return robotsNumberNear <= 2;
        }

        public bool isStationPotentiallyFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            var robotsNumberNear = 0;
            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.OwnerName == trueAuthor)
                {
                    var distance = DistanceHelper.FindDistance(robot.Position, cell);
                    if (distance <= 7)
                        robotsNumberNear++;
                }
            }
            return robotsNumberNear <= 2;
        }
    }
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }
}
