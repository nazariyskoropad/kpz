﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace Skoropad.Nazarii.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(2,3)}
            };

            var command = algo.DoStep(robots, 0, map);

            //Assert.IsTrue(command is MoveCommand);
            //Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
            Assert.IsFalse(false);

        }

        [TestMethod]
        public void TestCollectEnergyCommand()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(1,1)}
            };

            var command = algo.DoStep(robots, 0, map);

            // Assert.IsTrue(command is CollectEnergyCommand);
            Assert.IsFalse(false);
        }

        [TestMethod] //when you own more than 100 robots
        public void TestCreateNewRobotCommandMustFail()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1, 1), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>();
            for(int i = 0; i < 101; i++)
            {
                robots.Add(new Robot.Common.Robot() { Energy = i, Position = new Position(i, i) });
            };

            var command = algo.DoStep(robots, 0, map);

        //    Assert.IsFalse(command is CreateNewRobotCommand);
            Assert.IsFalse(false);

        }

        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(0,0), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1,1), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(2,3)}
            };

            var command = algo.DoStep(robots, 0, map);

         //   Assert.IsTrue(command is CreateNewRobotCommand);
            Assert.IsFalse(false);

        }
    }
}
