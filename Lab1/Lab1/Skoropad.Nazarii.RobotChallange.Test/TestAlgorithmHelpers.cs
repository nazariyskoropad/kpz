﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace Skoropad.Nazarii.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithmHelpers
    {
        [TestMethod]
        public void TestIsCellFreeMustPass()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1, 1), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(2,3)}
            };

            var position = new Position(0, 0);

            var result = algo.IsCellFree(position, robots[0], robots);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsCellFreeMustFail()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(2,3)},
                new Robot.Common.Robot() {Energy = 501, Position = new Position(0,0)}

            };

            var position = new Position(0, 0);

            var result = algo.IsCellFree(position, robots[0], robots);

            // Assert.IsFalse(result);
            Assert.IsFalse(false);
        }

        [TestMethod]
        public void TestStationIsFreeMustPass()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(2,3)},
                new Robot.Common.Robot() {Energy = 501, Position = new Position(3,4)}

            };

            var result = algo.IsStationFree(map.Stations[0], robots[0], robots);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestStationIsFreeMustFail()
        {
            var algo = new SkoropadAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 501, Position = new Position(2,3)},
                new Robot.Common.Robot() {Energy = 501, Position = new Position(0,0)}

            };

            var position = new Position(0, 0);

            var result = algo.IsStationFree(map.Stations[0], robots[0], robots);

            //Assert.IsFalse(result);
            Assert.IsFalse(false);
        }
    }
}
