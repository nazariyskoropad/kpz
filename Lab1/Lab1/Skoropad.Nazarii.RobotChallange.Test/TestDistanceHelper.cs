﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Skoropad.Nazarii.RobotChallange.Test
{
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestDistance()
        {
            // Arrange
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);

            //Act
            var distance = DistanceHelper.FindDistance(p1, p2);

            //Assert
            Assert.AreEqual(10, distance);
        }

        [DataTestMethod]
        [DataRow(8, 1, 2, 3, 4)]
        [DataRow(0, 2, 2, 2, 2)]
        [DataRow(0, 1, 1, 1, 1)]
        [DataRow(50, 0, 0, 5, 5)]
        [DataRow(34, 5, 7, 10, 10)]
        public void TestDistanceUsingDataRow(int result, int p1X, int p1Y, int p2X, int p2Y)
        {
            Position p1 = new Position(p1X, p1Y);
            Position p2 = new Position(p2X, p2Y);
            var distance = DistanceHelper.FindDistance(p1, p2);

            Assert.AreEqual(result, distance);
        }
    }
}
