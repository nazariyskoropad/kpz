﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab3
{
    public static class DictionaryExtension
    {
        public static void Write(this Dictionary<string, List<string>> dict)
        {
            foreach (var key in dict.Keys)
            {
                Console.WriteLine("Tournament Name : " + key);
                Console.Write("Teams: ");
                dict.GetValueOrDefault(key).ForEach(v => Console.Write(v + " "));
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
