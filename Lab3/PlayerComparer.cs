﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Lab3
{
    public class PlayerComparer : IComparer<Player>
    {
        public int Compare([AllowNull] Player x, [AllowNull] Player y)
        {
            if ((x == null && y == null) || (x.LastName == null && y.LastName == null)) return 0;
            if (x == null || x.LastName == null) return -1;
            if (y == null || y.LastName == null) return 1;
            return x.LastName.CompareTo(y.LastName);
        }
    }
}
