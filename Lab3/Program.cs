﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            var players = InitializaPlayers();
            var teams = InitializaTeams();
            var tournaments = InitializaTournaments();

            Part1(players, teams);
            Console.WriteLine();
            Part2(teams, tournaments);
            Console.WriteLine();
            Part3(players);
            Console.WriteLine();
            Part4(players);
        }

        private static void Part4(List<Player> players)
        {
            Console.WriteLine("Task 8 and additional task");
            var p = players.
                OrderByDescending(p => p.LastName).
                GroupBy(p => p.Country).
                OrderBy(g => g.Key);

            foreach (var group in p)
            {
                Console.WriteLine("Country " + group.Key);
                foreach (var el in group)
                {
                    Console.WriteLine(el.FirstName + " " + el.LastName);
                }
                Console.WriteLine();
            }

        }

        private static void Part3(List<Player> players)
        {
            Console.WriteLine("Tasks 6, 7");
            Console.WriteLine("Players sorted by last name");
            var sortedPlayersArray = players.ToArray();
            Array.Sort(sortedPlayersArray, new PlayerComparer());
            foreach (var el in sortedPlayersArray)
            {
                Console.WriteLine(el.LastName + " " + el.FirstName);
            }
        }

        private static void Part2(List<Team> teams, List<Tournament> tournaments)
        {
            Console.WriteLine("Tasks 3, 4 and additional task");
            var dict = new Dictionary<String, List<String>>();

            foreach (var tournament in tournaments)
            {
                dict.Add(tournament.Name, new List<string>());
            }

            foreach (var key in dict.Keys)
            {
                dict.GetValueOrDefault(key).
                    AddRange(teams.
                        Where(ts => ts.TournamentId == tournaments.
                                                First(tr => tr.Name == key).Id).
                        Select(ts => ts.Name).
                        OrderBy(n => n).
                        ToList());
            }

            dict.Write();
        }

        private static void Part1(List<Player> players, List<Team> teams)
        {
            var team = teams.First(t => t.Name == "Barcelona");
            var select = players.Where(p => p.TeamId == team.Id).Select(p => new
            {
                playerLastName = p.LastName,
                playerFirsttName = p.FirstName
            }).ToList();

            Console.WriteLine("Tasks 1, 2, 5 ");
            Console.WriteLine(team.Name + " players:");
            select.ForEach(s => Console.WriteLine(s.playerFirsttName + " " +s.playerLastName));
        }

        private static List<Tournament> InitializaTournaments()
        {
            return new List<Tournament>()
            {
                new Tournament() { Id = 1, Name = "La Liga", Country = "Spain"},
                new Tournament() { Id = 2, Name = "EPL", Country = "England"},
                new Tournament() { Id = 3, Name = "Seria A", Country = "Italy"},
                new Tournament() { Id = 4, Name = "Bundesliga", Country = "Germany"},
            };
        }

        private static List<Team> InitializaTeams()
        {
            return new List<Team>()
            {
                new Team() { Id = 1, Name = "Barcelona", TournamentId = 1},
                new Team() { Id = 2, Name = "Juventus", TournamentId = 3},
                new Team() { Id = 3, Name = "Westhem", TournamentId = 2},
                new Team() { Id = 4, Name = "Bayern", TournamentId = 4},
                new Team() { Id = 5, Name = "Liverpool", TournamentId = 2},
                new Team() { Id = 6, Name = "Real", TournamentId = 1},
                new Team() { Id = 7, Name = "Milan", TournamentId = 3},
            };
        }

        private static List<Player> InitializaPlayers()
        {
            return new List<Player>()
            {
                new Player() { Id = 1, FirstName = "Leo", LastName = "Messi", TeamId = 1, Country = "Argentina" },
                new Player() { Id = 5, FirstName = "Andreas", LastName = "Iniesta", TeamId = 1, Country = "Spain" },
                new Player() { Id = 2, FirstName = "Cristiano", LastName = "Ronaldo", TeamId = 2, Country = "Portugal" },
                new Player() { Id = 7, FirstName = "Paulo", LastName = "Dibala", TeamId = 2, Country = "Argentina" },
                new Player() { Id = 3, FirstName = "Anriy", LastName = "Iarmolenko", TeamId = 3, Country = "Ukraine" },
                new Player() { Id = 4, FirstName = "Tomas", LastName = "Muller", TeamId = 4, Country = "Germany" },
                new Player() { Id = 6, FirstName = "Sadio", LastName = "Mane", TeamId = 5, Country = "Senegal" },
                new Player() { Id = 8, FirstName = "Sergio", LastName = "Ramos", TeamId = 6, Country = "Spain" },
                new Player() { Id = 9, FirstName = "Zlatan", LastName = "Ibrahimovic", TeamId = 7, Country = "Sweden" },
            };       
        }
    }
}
