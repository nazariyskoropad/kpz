﻿using AutoMapper;
using Lab5.Models;
using Lab5.ViewModels;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5.Mapping
{
    public class Mapper
    {
        public readonly MapperConfiguration Config;

        public Mapper()
        {
            this.Config = new MapperConfiguration(configs =>
            {
                configs.CreateMap<DataModel, DataViewModel>().ReverseMap();
                configs.CreateMap <Product, ProductViewModel > ().ReverseMap();
                configs.CreateMap<Manufacturer, ManufacturerViewModel>().ReverseMap();
            });
        }

        public class MappingModule : NinjectModule
        {
            public override void Load()
            {
                Bind<IMapper>().ToMethod(context => new Mapper().Config.CreateMapper());
            }
        }
    }
}
