﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lab5.Constants;
using Lab5.Serializer;

namespace Lab5.Models
{
    public class DataModel
    {
        public IEnumerable<Manufacturer> Manufacturers { get; set; }
        public IEnumerable<Product> Products { get; set; }        

        public DataModel()
        {
            this.Manufacturers = new List<Manufacturer>();
            this.Products = new List<Product>();
        }

        public static DataModel Load()
        {
            return new DataModel()
            {
                Manufacturers = Lab5.Serializer.Serializer.LoadFromFile<List<Manufacturer>>(FilePath.Manufacturers),
                Products = Lab5.Serializer.Serializer.LoadFromFile<List<Product>>(FilePath.Products)
            };
        }
        public async Task Save()
        {
            await Task.WhenAll(new[]
            {
                Lab5.Serializer.Serializer.WriteIntoFile(FilePath.Manufacturers, this.Manufacturers),
                Lab5.Serializer.Serializer.WriteIntoFile(FilePath.Products, this.Products)
            });
        }
    }
}