﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5.Constants
{
    public static class FilePath
    {
        public const string Manufacturers = "Manufacturers.json";
        public const string Products = "Products.json";
    }
}
