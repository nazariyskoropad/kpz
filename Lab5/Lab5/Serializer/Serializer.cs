﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Lab5.Serializer
{
    public static class Serializer
    {
        public static T LoadFromFile<T>(string path)
        {
            using FileStream fs = new FileStream(path, FileMode.Open);
            using StreamReader reader = new StreamReader(fs);
            return JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
        }

        public static async Task WriteIntoFile<T>(string path, T data)
        {
            string json = JsonConvert.SerializeObject(data);
            await File.WriteAllTextAsync(path, json);
        }

        internal static Task WriteIntoFile(object restaurantPath, object restaurants)
        {
            throw new NotImplementedException();
        }
    }
}
