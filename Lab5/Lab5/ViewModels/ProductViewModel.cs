﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5.ViewModels
{
    public class ProductViewModel : ViewModel
    {
        private int _id;
        private string _name;
        private string _description;
        private decimal _cost;
        private int _manufacturerId;
        private string _image;

        public int Id
        {
            get => this._id;
            set { this._id = value; NotifyPropertyChanged(nameof(Id)); }
        }

        public string Name
        {
            get => this._name;
            set { this._name = value; NotifyPropertyChanged(nameof(Name)); }
        }

        public string Description
        {
            get => this._description;
            set { this._description = value; NotifyPropertyChanged(nameof(Description)); }
        }

        public decimal Cost
        {
            get => this._cost;
            set { this._cost = value; NotifyPropertyChanged(nameof(Cost)); }
        }


        public int ManufacturerId
        {
            get => this._manufacturerId;
            set { this._manufacturerId = value; NotifyPropertyChanged(nameof(ManufacturerId)); }
        }

        public string Image
        {
            get => this._image;
            set { this._image = value; NotifyPropertyChanged(nameof(Image)); }
        }
    }
}
