﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Lab5.ViewModels
{
    public class ViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
