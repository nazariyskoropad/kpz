﻿using Lab5.Actions;
using Lab5.Constants;
using Lab5.Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab5.ViewModels
{
    public class DataViewModel : ViewModel
    {
        private ObservableCollection<ManufacturerViewModel> _manufacturersViewModels;
        private ObservableCollection<ProductViewModel> _productsViewModels;

        public ICommand DeleteProductCommand { get; set; }
        public ICommand SelectProductCommand { get; set; }

        public DataViewModel()
        {
            this.DeleteProductCommand = new Command(async (object parameter) =>
            {
                this.Products.Remove(parameter as ProductViewModel);
                await Save();
            });
        }

        public ObservableCollection<ManufacturerViewModel> Manufacturers
        {
            get => _manufacturersViewModels;
            set
            {
                _manufacturersViewModels = value;
                NotifyPropertyChanged(nameof(Manufacturers));
            }
        }

        public ObservableCollection<ProductViewModel> Products
        {
            get => _productsViewModels;
            set
            {
                this._productsViewModels = value;
                this.NotifyPropertyChanged(nameof(this.Products));
            }
        }

        private async Task Save()
        {
            await Task.WhenAll(new[]
            {
                Serializer.Serializer.WriteIntoFile(FilePath.Manufacturers, this.Manufacturers),
                 Serializer.Serializer.WriteIntoFile(FilePath.Products, this.Products)
            });
        }
    }
}