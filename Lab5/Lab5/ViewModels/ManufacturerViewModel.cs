﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5.ViewModels
{
    public class ManufacturerViewModel : ViewModel
    {
        private int _id;
        private string _name;
        private string _description;
        private string _country;
        private string _image;

        public int Id
        {
            get => this._id;
            set { this._id = value; NotifyPropertyChanged(nameof(Id)); }
        }

        public string Name
        {
            get => this._name;
            set { this._name = value; NotifyPropertyChanged(nameof(Name)); }
        }

        public string Description
        {
            get => this._description;
            set { this._description = value; NotifyPropertyChanged(nameof(Description)); }
        }

        public string Country
        {
            get => this._country;
            set { this._country = value; NotifyPropertyChanged(nameof(Country)); }
        }

        public string Image
        {
            get => this._image;
            set { this._image = value; NotifyPropertyChanged(nameof(Image)); }
        }
    }
}
