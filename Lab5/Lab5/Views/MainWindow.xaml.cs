﻿using AutoMapper;
using Lab5.Models;
using Lab5.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Lab5.Mapping.Mapper;

namespace Lab5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IMapper _mapper;
        private DataViewModel _viewModel;
        private DataModel _model;

        public MainWindow()
        {
            try
            {
                var kernel = new StandardKernel(new MappingModule());
                _mapper = kernel.Get<IMapper>();
                _model = DataModel.Load();
                _viewModel = _mapper.Map<DataModel, DataViewModel>(_model);
                this.DataContext = _viewModel;

                InitializeComponent();
                TurnOnMusic();
            }
            catch (Exception ex)
            {}   
        }

        private void TurnOnMusic()
        {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.Volume = 50;
            mediaPlayer.Open(new Uri(@"D:\1univer\Semester_5\KPZ\Lab5Nazik\Lab5\sound.mp3"));
            mediaPlayer.Play();
        }
    }
}
